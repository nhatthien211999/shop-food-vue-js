import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import category from "./modules/categories";
import product from "./modules/products"
import component from "./modules/components" 

export default new Vuex.Store({
  modules: {
      category,
      product,
      component
  }
});