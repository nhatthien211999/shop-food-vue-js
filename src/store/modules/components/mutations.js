export const SET_COMPONENT = (state, component) => {
    Object.keys(state.active).forEach(key => state.active[key] = false)
    state.active[component] = true
}