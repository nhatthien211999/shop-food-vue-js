export const getComponent = ({commit}, component) => {
    commit('SET_COMPONENT', component);
}