import Category from "../../../api/Category.js"


export const getCategories = ({commit}) => {
    Category.all().then((response) => {
        let categories = response.data.data;
        commit('SET_CATEGORIES', categories);
    });
}
