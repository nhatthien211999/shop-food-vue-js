import * as getters from './getters'
import * as actions from './actions'
import * as mutations from './mutations'


const state = {
    categories: [],
};
const namespaced = true;

export default {
    namespaced,
    state,
    getters,
    actions,
    mutations
}