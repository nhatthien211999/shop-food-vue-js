import Product from "../../../api/Product"


export const getProducts = ({commit}) => {
    Product.all().then((response) => {
        let products = response.data.data;
        commit('SET_PRODUCTS', products);
    });
}

export const getProduct = ({ commit }, productId) => {
    
    Product.show(productId).then(response => {
        commit('SET_PRODUCT', response.data.data);
    })
}