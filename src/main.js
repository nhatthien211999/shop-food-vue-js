import Vue from 'vue'
import App from './App.vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

import router from './router'
import store from './store'

import jQuery from 'jquery'

Vue.use(VueAxios, axios)

window.$ = window.jQuery = jQuery;

new Vue({
  render: h => h(App),
  store,
  router, 
}).$mount('#app')


